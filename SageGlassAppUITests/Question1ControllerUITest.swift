//
//  Question1ControllerUITest.swift
//  SageGlassApp
//
//  Created by Henry Yam on 5/10/17.
//  Copyright © 2017 DrexelSeniorDesign. All rights reserved.
//

import XCTest
@testable import SageGlassApp

class Question1ControllerUITest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        
        continueAfterFailure = false
        
        var app = XCUIApplication()
        app.launch()
    }
    
    func setup(vcName: String) -> UIViewController{
        let bundle = Bundle(for: type(of: self))
        let storyboard = UIStoryboard(name: "Main", bundle: bundle)
        var vc = UIViewController()
        if(vcName == "q1"){
            vc = storyboard.instantiateViewController(withIdentifier: "Question1Controller") as! Question1Controller
            _ = vc.view
        }
        else if(vcName == "home"){
            vc = storyboard.instantiateViewController(withIdentifier: "HomeController") as! HomeController
            _ = vc.view
        }
        return vc
    
    }
    
    func startSurvey(){
        
    }
    
}
