//
//  q1cUITest.swift
//  SageGlassApp
//
//  Created by Henry Yam on 5/10/17.
//  Copyright © 2017 DrexelSeniorDesign. All rights reserved.
//

import XCTest
@testable import SageGlassApp

class q1cUITest: XCTestCase {
    
    let app = XCUIApplication()
    var home: HomeController!
    var q1c: Question1Controller!
    
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        app.launch()
        let sgb = Bundle(path: "/Users/hky/Library/Developer/CoreSimulator/Devices/ACA111AB-906B-497B-B388-EA3A318D2C21/data/Containers/Bundle/Application/9CF21C25-8DE6-47F0-B38A-66D13D2568A0/SageGlassApp.app")
        let main = UIStoryboard(name: "Main", bundle: sgb)
        var home = HomeController(nibName: "HomeController", bundle: sgb)

        //q1c = main.instantiateViewController(withIdentifier: "Question1Controller.swift") as! Question1Controller
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        XCTAssertEqual(1, 1)
    }
    
}
