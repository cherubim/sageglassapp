//
//  ControlCellCollectionViewCell.swift
//  SageGlassApp
//
//  Created by Alex Baryschpolec on 4/19/17.
//  Copyright © 2017 DrexelSeniorDesign. All rights reserved.
//

import UIKit

class ControlCellCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var checkedImg: UIImageView!
    var value: Int = -1
    @IBOutlet weak var spinner: UIActivityIndicatorView!
}
