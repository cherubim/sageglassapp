//
//  Question4Controller.swift
//  SageGlassApp
//
//  Created by Kaylee Stutts on 4/6/17.
//  Copyright © 2017 DrexelSeniorDesign. All rights reserved.
//

import UIKit

class Question4Controller: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var mcQLabel: UILabel!
    @IBOutlet weak var mcLabel: UILabel!
    @IBOutlet weak var mcOtherField: UITextField!
    @IBOutlet weak var mcOtherLabel: UILabel!
    
    let reuseIdentifier = "cell" // also enter this string as the cell identifier in the storyboard
    var imageNames = ["eye", "on_switch", "off_switch", "window", "moving", "nothing"]
    var items = [UIImage(named: "eye")!, UIImage(named: "on_switch")!, UIImage(named: "off_switch")!, UIImage(named: "window")!, UIImage(named: "moving")!, UIImage(named: "null_set")!]
    var toPass:String!
    var selectedValues = [Int]()
    var subString = String()
    
    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! Q4CollectionViewCell
        
        // Use the outlet in our custom class to get a reference to the UIImage in the cell
        cell.mcIconView.image = self.items[indexPath.item]
        
        // borders
        cell.layer.borderColor = UIColor.black.cgColor
        cell.layer.borderWidth = 2
        
        // corners
        cell.layer.cornerRadius = 8
        
        // shadows
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOffset = CGSize(width: 3, height: 3)
        cell.layer.shadowOpacity = 0.7
        cell.layer.shadowRadius = 4.0
        
        //cell.backgroundColor = UIColor.cyan
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        
        collectionView.allowsMultipleSelection = true
        
        let cell = collectionView.cellForItem(at: indexPath)!
            
        cell.contentView.backgroundColor = UIColor.cyan
        
        mcLabel.text = (survey.questions[3] as! MCQuestion).choices[indexPath.item]
        
        selectedValues.append(indexPath.item)
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.cellForItem(at: indexPath)!
        cell.contentView.backgroundColor = UIColor.clear
        
        let indexOfDeselItem = selectedValues.index(of: indexPath.item)
        selectedValues.remove(at: indexOfDeselItem!)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        nextButton.layer.shadowOpacity = 0.7
        nextButton.layer.shadowOffset = CGSize(width: 3.0, height: 2.0)
        nextButton.layer.cornerRadius = 5

        let choices = [
            "I closed my eyes",
            "I or a family member turned the lights on",
            "I or a family member turned the lights off",
            "I or a family member adjusted the blinds/adjusted the tint of glass",
            "I moved my position in the room",
            "I and my family members did nothing"]
        
        survey.questions[3].choices = choices
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        mcOtherField.returnKeyType = UIReturnKeyType.done // Make return key say 'Done'
        
    }

    override func shouldPerformSegue(withIdentifier identifier: String?, sender: Any?) -> Bool {
        var doSegue = false
        if selectedValues.count > 0 || !(mcOtherField.text?.isEmpty)!{
            doSegue = true
        }
        else{
            mcLabel.text = "Please make a selection"
            shakeText()
        }
        return doSegue
    }
    
    func shakeText() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0, options: [.curveLinear], animations: {
            self.mcLabel.center = CGPoint(x: self.mcLabel.center.x, y: self.mcLabel.center.y - 20)
        }, completion: nil)
        UIView.animate(withDuration: 0.5, delay: 0.5, usingSpringWithDamping: 0.2, initialSpringVelocity: 0, options: [.curveLinear], animations: {
            self.mcLabel.center = CGPoint(x: self.mcLabel.center.x, y: self.mcLabel.center.y + 20)
        }, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func saveData(_ sender: Any) {
        //machine-readable answers are all the selected values that were gathered from the collectionView's didSelectItem 
        //method
        let machineAnswers = selectedValues
        var humanAnswers = [String()]
        
        //loop through all the selected values and use them to append the appropriate choice to the humanAnswer array
        for i in 0..<selectedValues.count {
            if i == 0 {
                humanAnswers[i] = (survey.questions[3].choices[machineAnswers[i]])
            }
            else{
                humanAnswers.append(survey.questions[3].choices[machineAnswers[i]])
            }
        }
        
        //append other field
        let otherText = mcOtherField.text
        if(!(otherText?.isEmpty)!){
            if humanAnswers[0] == ""{
                humanAnswers[0] = otherText!
            }
            else{
                humanAnswers.append(otherText!)
            }
        }
        
        //save answers
        let q4 = survey.questions[3] as! MCQuestion
        q4.saveAnswers(answers: humanAnswers)
        
        //save to survey
        survey.logData()
    }
    
}
