//
//  Question1Controller.swift
//  SageGlassApp
//
//  Created by Alex Baryschpolec on 3/3/17.
//  Copyright © 2017 DrexelSeniorDesign. All rights reserved.
//

import UIKit

class Question1Controller: UIViewController {
    
    @IBOutlet weak var sliderLabel: UILabel!
    @IBOutlet weak var q1Label: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var tempImgView: UIImageView!
    @IBOutlet weak var thermImg: UIImageView!
    var subString = String()
    var failedAttempts = 0
    
    @IBOutlet weak var heatslider: UISlider!{
        didSet{
            heatslider.transform = CGAffineTransform(rotationAngle: CGFloat(-Double.pi/2))
        }
    }
    
    var images: [UIImage?]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        images = [UIImage(named: "Frost_Frame51")!, UIImage(named: "Frost_Frame46")!, UIImage(named: "Frost_Frame41")!, UIImage(named: "Frost_Frame36")!, UIImage(named: "Frost_Frame31")!,
                 UIImage(named: "Frost_Frame26")!, UIImage(named: "Frost_Frame21")!, UIImage(named: "Frost_Frame16")!, UIImage(named: "Frost_Frame11")!, UIImage(named: "Frost_Frame6")!,
                 UIImage(named: "Frost_Frame1")!, UIImage(named: "HEAT_00015")!, UIImage(named: "HEAT_00015")!, UIImage(named: "HEAT_00030")!, UIImage(named: "HEAT_00045")!, UIImage(named: "HEAT_00060")!, UIImage(named: "HEAT_00075")!, UIImage(named: "HEAT_00090")!, UIImage(named: "HEAT_00105")!, UIImage(named: "HEAT_00120")!, UIImage(named: "HEAT_00135")!, UIImage(named: "HEAT_00149")!]
        
        //view.backgroundColor = UIColor(patternImage: UIImage(named: "HEAT_00015.png")!) // Default Background Image
        tempImgView.image = UIImage(named: "HEAT_00015")!
        let choices = ["Too Cold", "A Little Cold", "Just Right", "A Little Hot", "Too Hot"]
        survey.questions[0].choices = choices
        
        nextButton.layer.shadowOpacity = 0.7
        nextButton.layer.shadowOffset = CGSize(width: 3.0, height: 2.0)
        nextButton.layer.cornerRadius = 5
        
        thermImg.layer.shadowOpacity = 0.7
        thermImg.layer.shadowOffset = CGSize(width: 3.0, height: 2.0)


    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showAlert(title: String, message: String, actions: [String]){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        for i in 0..<actions.count{
            alert.addAction(UIAlertAction(title: actions[i], style: UIAlertActionStyle.default, handler: nil))
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func sliderValueChanged(_ sender: Any){
        
        // Get the required background image for this value
        var index = Int(heatslider.value.rounded());
        if index > Int(heatslider.maximumValue){
            index = Int(heatslider.maximumValue)
        }
        else if index < Int(heatslider.minimumValue){
            index = Int(heatslider.minimumValue)
        }
        tempImgView.image = images[index]
        
        // Change the caption that describes what value the user is inputting
        // 5 levels of granularity, 21 values on slider -> 21 / 5 = 4
        if index >= 4 {
            sliderLabel.text = survey.questions[0].choices[(index/4)-1]
        }
        else{
            sliderLabel.text = survey.questions[0].choices[0]
        }
    }
    
    // Upon clicking Next, add data to CSV and move to next view
    // Use switch statement to get appropriate integer value from slider label
    @IBAction func saveData(_ sender: Any) {
        let sliderText = sliderLabel.text!
        let q1 = survey.questions[0] as! SliderQuestion
        if(q1.choices.index(of: sliderText) == nil){
            if(failedAttempts == 0){
                showAlert(title: "Uh-oh!", message: "The answer that was received was not a part of the choices. Please enter in your answer again.", actions: ["OK"])
            }
            else{
                showAlert(title: "Uh-oh!", message: "We're still getting a different answer than expected. Please let somebody know about this.", actions: ["OK"])
                q1.saveAnswers(answer: sliderText)
                self.performSegue(withIdentifier: "q1ToQ2", sender: self)
            }
        }
        else{
            q1.saveAnswers(answer: sliderText)
            self.performSegue(withIdentifier: "q1ToQ2", sender: self)
        }
    }
    
}

