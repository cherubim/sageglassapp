//
//  Settings.swift
//  SageGlassApp
//
//  Created by Henry Yam on 3/12/17.
//  Copyright © 2017 DrexelSeniorDesign. All rights reserved.
//

import UIKit
import MessageUI

class Settings: UITableViewController, UIPickerViewDelegate, UIPickerViewDataSource, MFMailComposeViewControllerDelegate{
    
    @IBOutlet weak var emailAddr: UITextField!
    @IBOutlet weak var surveysPerDay: UIPickerView!
    @IBOutlet weak var startTime: UIDatePicker!
    @IBOutlet weak var endTime: UIDatePicker!
    @IBOutlet weak var adminPass: UITextField!
    @IBOutlet weak var controlRoom: UISwitch!
    
    let frequency = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]
    let dh = DataHandler()
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        surveysPerDay.dataSource = self
        surveysPerDay.delegate = self
        emailAddr.text = config?.destEmail
        surveysPerDay.selectRow((config?.perDay)!, inComponent: 0, animated: false)
        startTime.setDate((config?.startTime)!, animated: false)
        endTime.setDate((config?.endTime)!, animated: false)
        adminPass.isSecureTextEntry = true
        adminPass.text = config?.password
        
        controlRoom.setOn((config?.controlRoom)!, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        var height = 44
        if indexPath.section == 0{
            if indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2{
                height = 135
            }
        }
        return CGFloat(height)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: Data Sources
    func numberOfComponents(in: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return frequency.count
    }
    
    //MARK: Delegates
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return frequency[row]
    }

    @IBAction func switchRoom(_ sender: UISwitch) {
        config?.controlRoom = sender.isOn
    }
    
    @IBAction func saveSettings(_ sender: Any) {
        config?.perDay = surveysPerDay.selectedRow(inComponent: 0)
        config?.destEmail = emailAddr.text!
        config?.startTime = startTime.date
        config?.endTime = endTime.date
        config?.controlRoom = controlRoom.isOn
        config?.password = adminPass.text!
        config?.saveSettings()
    }
    
    //MARK: Mail Stuff
    @IBAction func exportData(_ sender: Any) {
        dh.sendEmail(vc: self)
    }
    
    @IBAction func reset(_ sender: Any) {
        config?.setDefault()
        
        surveysPerDay.dataSource = self
        surveysPerDay.delegate = self
        emailAddr.text = config?.destEmail
        surveysPerDay.selectRow((config?.perDay)!, inComponent: 0, animated: false)
        startTime.setDate((config?.startTime)!, animated: false)
        endTime.setDate((config?.endTime)!, animated: false)
        adminPass.isSecureTextEntry = true
        adminPass.text = config?.password
        controlRoom.setOn((config?.controlRoom)!, animated: true)
        
        let alert = UIAlertController(title: "Reset", message: "All Settings Reset", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    /*
    func showSendMailErrorAlert() {
        let alertTitle = "Could not send e-mail"
        let alertMessage = "Your device is unable to send e-mail messages. Please check your e-mail configuration in Settings."
        let alertStyle = UIAlertControllerStyle.alert
        let alertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
        
        let alertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: alertStyle)
        alertController.addAction(alertAction)
        self.present(alertController, animated:true, completion:nil)
    }
 */
}
