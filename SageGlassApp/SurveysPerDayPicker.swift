//
//  SurveysPerDayPicker.swift
//  SageGlassApp
//
//  Created by Henry Yam on 3/12/17.
//  Copyright © 2017 DrexelSeniorDesign. All rights reserved.
//

import UIKit

class SurveysPerDayPicker: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource{
    @IBOutlet weak var frequencyPicker: UIPickerView!
    var frequencyPicked: Int!
    let pickerData = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        frequencyPicker.dataSource = self
        frequencyPicker.delegate = self
    }
    
    //MARK: Data Sources
    func numberOfComponents(in: UIPickerView) -> Int {
        return 11
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    //MARK: Delegates
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let output = String(pickerData[row])
        return output
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
