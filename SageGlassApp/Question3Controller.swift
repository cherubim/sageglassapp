//
//  Question3Controller.swift
//  SageGlassApp
//
//  Created by Henry Yam on 4/8/17.
//  Copyright © 2017 DrexelSeniorDesign. All rights reserved.
//

import UIKit

class Question3Controller: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var q3Label: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    
    let reuseIdentifier = "q3cell" // also enter this string as the cell identifier in the storyboard
    var imageNames = ["frown", "smile"]
    var items = [UIImage(named: "frown")!, UIImage(named: "smile")!]
    var labels = ["Yes", "No"]
    var selectedCell = Int()
    var toPass:String!
    var subString = String()
    var cellWasSelected = false
    var failedAttempts = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        nextButton.layer.shadowOpacity = 0.7
        nextButton.layer.shadowOffset = CGSize(width: 3.0, height: 2.0)
        nextButton.layer.cornerRadius = 5
        
        let choices = labels
        survey.questions[2].choices = choices
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let q3cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! Q3CollectionViewCell
        
        // Use the outlet in our custom class to get a reference to the UIImage in the cell
        q3cell.smileIconView.image = self.items[indexPath.item]
        q3cell.smileIconLabel.text = self.labels[indexPath.item]
        
        // borders
        q3cell.layer.borderColor = UIColor.black.cgColor
        q3cell.layer.borderWidth = 2
        
        // corners
        q3cell.layer.cornerRadius = 8
        
        // shadows
        q3cell.layer.shadowColor = UIColor.black.cgColor
        q3cell.layer.shadowOffset = CGSize(width: 3, height: 3)
        q3cell.layer.shadowOpacity = 0.7
        q3cell.layer.shadowRadius = 4.0
        
        //cell.backgroundColor = UIColor.cyan // make cell more visible in our example project
        
        return q3cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        // handle tap events
        
        collectionView.allowsMultipleSelection = false
        
        let cell = collectionView.cellForItem(at: indexPath)!
        selectedCell = indexPath.item
        cell.contentView.backgroundColor = UIColor.cyan
        cellWasSelected = true
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.cellForItem(at: indexPath)!
        cell.contentView.backgroundColor = UIColor.clear
        cellWasSelected = false
    }
    
    func shakeText() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0, options: [.curveLinear], animations: {
            self.q3Label.center = CGPoint(x: self.q3Label.center.x, y: self.q3Label.center.y - 20)
        }, completion: nil)
        UIView.animate(withDuration: 0.5, delay: 0.5, usingSpringWithDamping: 0.2, initialSpringVelocity: 0, options: [.curveLinear], animations: {
            self.q3Label.center = CGPoint(x: self.q3Label.center.x, y: self.q3Label.center.y + 20)
        }, completion: nil)
    }
    
    func showAlert(title: String, message: String, actions: [String]){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        for i in 0..<actions.count{
            alert.addAction(UIAlertAction(title: actions[i], style: UIAlertActionStyle.default, handler: nil))
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func q4OrEnd(_ sender: Any) {
        if cellWasSelected == false {
            shakeText()
        }
        else if labels[selectedCell] == "No" {
            survey.logData()
            self.performSegue(withIdentifier: "q3ToEnd", sender: self)
        }
        else if labels[selectedCell] == "Yes" {
            self.performSegue(withIdentifier: "q3ToQ4", sender: self)
        }

    }
    
    
    @IBAction func saveData(_ sender: Any) {
        let q3 = survey.questions[2] as! MCQuestion
        let selectedCellText = survey.questions[2].choices[selectedCell]
        var answerArr = [String]()
        answerArr.append(selectedCellText)
        if(q3.choices.index(of: selectedCellText) == nil){
            if(failedAttempts == 0){
                showAlert(title: "Uh-oh!", message: "The answer that was received was not a part of the choices. Please enter in your answer again.", actions: ["OK"])
                failedAttempts += 1
            }
            else{
                showAlert(title: "Uh-oh!", message: "We're still getting a different answer than expected. Please let somebody know about this.", actions: ["OK"])
                q3.saveAnswers(answer: selectedCellText)
                q4OrEnd(sender: self)
            }
        }
        else{
            var machineAnswerArr = [Int]()
            let answer = answerArr[0]
            if answer == "Yes"{
                machineAnswerArr.append(1)
            }
            else if answer == "No"{
                machineAnswerArr.append(0)
            }
            q3.saveMAnswers(mAnswers: machineAnswerArr)
            q3.saveHAnswers(hAnswers: answerArr)
            q4OrEnd(sender: self)
        }
    }
}
