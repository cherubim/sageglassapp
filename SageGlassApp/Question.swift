//
//  Question.swift
//  SageGlassApp
//
//  Created by Alex Baryschpolec on 3/10/17.
//  Copyright © 2017 DrexelSeniorDesign. All rights reserved.
//

import Foundation

class Question: NSObject{
    var question: String
    var machineAnswer: Int;
    var humanAnswer: String;
    var choices: [String];
    var machineAnswerStr: String;
    var humanAnswerStr: String;
    
    init(question: String){
        self.question = question;
        self.machineAnswer = Int()
        self.humanAnswer = String()
        self.choices = [String()]
        self.machineAnswerStr = String()
        self.humanAnswerStr = String()
    }
    
    func saveMAnswer(mAnswer: Int){
        machineAnswer = mAnswer
    }
    
    func saveHAnswer(hAnswer: String){
        humanAnswer = hAnswer
    }
    
    func saveAnswers(answer: String){
        let machineAnswer = choices.index(of: answer)
        let humanAnswer = answer
        
        if(machineAnswer == nil){
            saveMAnswer(mAnswer: -1)
        }
        else{
            saveMAnswer(mAnswer: machineAnswer!)
        }
        saveHAnswer(hAnswer: humanAnswer)
        
        machineAnswerStr = formMachineAnswer()
        humanAnswerStr = formHumanAnswer()
    }
    
    func formMachineAnswer() -> String{
        return "\t" + String(machineAnswer)
    }
    
    func formHumanAnswer() -> String{
        return "\t" + humanAnswer
    }
    
    func getAnswers() -> String{
        let answer = machineAnswerStr + humanAnswerStr
        return answer
    }
}
