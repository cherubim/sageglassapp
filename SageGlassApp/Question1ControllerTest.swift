//
//  Question1ControllerTest.swift
//  SageGlassApp
//
//  Created by Henry Yam on 5/7/17.
//  Copyright © 2017 DrexelSeniorDesign. All rights reserved.
//

import XCTest
@testable import SageGlassApp

class Question1ControllerTest: XCTestCase {
    
    var survey = Survey()
    var q1 = Question(question: "How is the temperature in the room now?")
    
    func setup(){
        q1.choices = ["Too Cold", "A Little Cold", "Just Right", "A Little Hot", "Too Hot"]
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testq1DataSavingCorrectInput(){
        setup()
        (q1 as! SliderQuestion).saveAnswers(answer: "A Little Hot")
        XCTAssertEqual(q1.machineAnswer, 3)
    }
    
    func testq1DataSavingIncorrectInput(){
        
    }
    
    func testBase(){
        XCTAssert(1 == 1)
    }
}
