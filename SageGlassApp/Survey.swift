//
//  Survey.swift
//  SageGlassApp
//
//  Created by Alex Baryschpolec on 3/10/17.
//  Copyright © 2017 DrexelSeniorDesign. All rights reserved.
//

import UIKit

class Survey: NSObject{
    var questions: [Question]
    
    override init(){
        print("Initializing Survey")
        self.questions = [];
        var question1: SliderQuestion = SliderQuestion.init(question: "How is the temperature in the room now?");
        var question2: SliderQuestion = SliderQuestion.init(question: "How is the light coming in through the windows?");
        var question3: MCQuestion = MCQuestion.init(question: "Has the sunlight been bothering you?");
        var question4: MCQuestion = MCQuestion.init(question: "What did you do to make it better?");
        self.questions = [question1, question2, question3, question4];
    }
    
    func getTimeStamp() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateStyle = DateFormatter.Style.medium
        var calDate = formatter.string(from: date)
        var calDateArr = calDate.components(separatedBy: ",")
        calDate = calDateArr[0] + calDateArr[1]
        formatter.timeStyle = DateFormatter.Style.short
        formatter.dateStyle = DateFormatter.Style.none
        let time = formatter.string(from: date)
        let timeStamp = calDate + " " + time
        return timeStamp
    }
    
    func getQuestionHeader() -> String {
        let header = "Date\t" + "Q1 Machine\t" + "Q1 Human\t" + "Q2 Machine\t" + "Q2 Human\t" + "Q3 Machine\t" + "Q3 Human\t" + "Q4 Machine\t" + "Q4 Human"
        return header
    }
    
    func gatherSurveyData() -> String{
        var row = String()
        for i in 0..<questions.count{
            let component = questions[i].getAnswers()
            
            row += component
        }
        row += "\n"
        return row
    }
    
    func logData(){
        print("Logging data...")
        let dh = DataHandler()
        if dh.checkFileExists(fileName: (config?.outFile)!){
            print("outFile exists")
            let row = getTimeStamp() + gatherSurveyData()
            dh.save(subString: row, fileName: (config?.outFile)!)
        }
        else{
            print("outFile doesn't exist. I should be creating it")
            let row = getQuestionHeader() + "\n" + getTimeStamp() + gatherSurveyData()
            dh.save(subString: row, fileName: (config?.outFile)!)
        }
    }
}
