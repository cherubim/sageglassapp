//
//  DataHandler.swift
//  SageGlassApp
//
//  Created by Henry Yam on 4/9/17.
//  Copyright © 2017 DrexelSeniorDesign. All rights reserved.
//

import Foundation
import MessageUI

class DataHandler: UIViewController, MFMailComposeViewControllerDelegate{
    var file = config?.outFile
    var emailAddr = config?.destEmail
    
    func getPath() -> URL{
        var path:URL? = nil
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            path = dir
        }
        return path!
    }
    
    func save(subString: String, fileName:String){
        let fm = FileManager()
        let home = getPath()
        let filePath = home.appendingPathComponent(fileName)
        let backup = home.appendingPathComponent(fileName + "-bak")
        do {
            if checkFileExists(fileName: fileName){
                print("File exists. Appending to it now...")
                //assign the file to the file handler
                let fh = try FileHandle(forUpdating: filePath)
                //make a copy of the file
                try fm.copyItem(at: filePath, to: backup)
                //position file handler to the end of the file
                fh.seekToEndOfFile()
                //convert output string to Data object
                let data = subString.data(using: String.Encoding.utf8)
                //filehandler shall write data at the position that it went to (at the end)
                fh.write(data!)
                //remove copy
                try fm.removeItem(at: backup)
                //if filename has the string "-bak" in it, that means we're writing to the backup file
                //this means that something previously went wrong with the original copy and now we want to make the
                //backup file the original.
                //Check if -bak exists in the name of the file. If it does, remove the "-bak" part and convert it into a URL
                //"Move" the backup file to the same directory, but with the new name (without the -bak part)
                if fileName.range(of: "-bak") != nil{
                    let newName = fileName.replacingOccurrences(of: "-bak", with: "")
                    let newFilePath = home.appendingPathComponent(newName)
                    try fm.moveItem(at: filePath, to: newFilePath)
                }
            }
            else{
                print("File didn't exist. Creating file now...")
                try subString.write(to: filePath, atomically: false, encoding:String.Encoding.utf8)
            }
        }
        catch {
            print("Error occurred while writing to file: \(error)")
            if checkFileExists(fileName: fileName + "-bak"){
                save(subString: subString, fileName: fileName + "-bak")
            }
        }
    }
    
    func getData(fileName: String) -> Data{
        let home = getPath()
        let filePath = home.appendingPathComponent(fileName)
        var data = Data()
        do{
            let dataString = try String(contentsOf: filePath, encoding: String.Encoding.utf8)
            let dataNSString = dataString as NSString
            data = dataNSString.data(using: String.Encoding.utf8.rawValue)!
        }
        catch{
            print("Error reading file from path: \(error)")
        }
        return data
    }
    
    func getDataString(fileName: String) -> String{
        let home = getPath()
        let filePath = home.appendingPathComponent(fileName)
        var dataString = String()
        do{
            dataString = try String(contentsOf: filePath, encoding: String.Encoding.utf8)
        }
        catch{
            print("Error reading file from path: \(error)")
        }
        return dataString
    }
    
    func checkFileExists(fileName: String) -> Bool{
        print("Checking if " + fileName + " exists...")
        var fileExists = false;
        let home = getPath()
        let filePath = home.appendingPathComponent(fileName).path
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: filePath) {
            fileExists = true
        }
        return fileExists
    }
    
    func configureEmail() -> MFMailComposeViewController{
        let mailComposerVC = MFMailComposeViewController()
        let dataHandler = DataHandler()
        let data = dataHandler.getData(fileName: file!)
        var emailArr = emailAddr?.components(separatedBy: ";")
        var index = 0
        for addr in emailArr!{
            emailArr?[index] = addr.trimmingCharacters(in: .whitespaces)
            index += 1
        }
        mailComposerVC.mailComposeDelegate = self
        mailComposerVC.setToRecipients([emailAddr!])
        mailComposerVC.setSubject("SageGlass Data  " + (config?.outFile)!)
        mailComposerVC.addAttachmentData(data as Data, mimeType: "text/tab-separated-values", fileName: (config?.outFile)!)
        return mailComposerVC
    }
    
    func sendEmail(vc: UIViewController){
        if MFMailComposeViewController.canSendMail() {
            let emailClient = configureEmail()
            vc.show(emailClient, sender: vc)
        }
        else{
            print("Unable to send emails")
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        if result == MFMailComposeResult.sent{
            deleteFile(fileName: file!)
        }
        
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }
    
    func overwriteWith(subString: String, fileName: String){
        let fm = FileManager()
        let home = getPath()
        let filePath = home.appendingPathComponent(fileName)
        let backup = home.appendingPathComponent(fileName + "-bak")
        do {
            if checkFileExists(fileName: fileName){
                //make a copy of the file
                try fm.copyItem(at: filePath, to: backup)
                //remove the file
                try fm.removeItem(at: filePath)
                //write the string out to a file
                try subString.write(to: filePath, atomically: false, encoding: String.Encoding.utf8)
                //remove copy
                try fm.removeItem(at: backup)
                //if filename has the string "-bak" in it, that means we're writing to the backup file
                //this means that something previously went wrong with the original copy and now we want to make the
                //backup file the original.
                //Check if -bak exists in the name of the file. If it does, remove the "-bak" part and convert it into a URL
                //"Move" the backup file to the same directory, but with the new name (without the -bak part)
                if fileName.range(of: "-bak") != nil{
                    let newName = fileName.replacingOccurrences(of: "-bak", with: "")
                    let newFilePath = home.appendingPathComponent(newName)
                    try fm.moveItem(at: filePath, to: newFilePath)
                }
            }
            else{
                try subString.write(to: filePath, atomically: false, encoding:String.Encoding.utf8)
            }
        }
        catch {
            print("Error occurred while writing to file: \(error)")
            if checkFileExists(fileName: fileName + "-bak"){
                save(subString: subString, fileName: fileName + "-bak")
            }
        }
    }
    
    func deleteFile(fileName: String){
        let fm = FileManager()
        let home = getPath()
        let filePath = home.appendingPathComponent(fileName)
        do{
            try fm.removeItem(at: filePath)
            print(fileName + " has been deleted...")
        }
        catch{
            print("Error occurred while trying to delete " + fileName + " : \(error)")
        }
    }

}
