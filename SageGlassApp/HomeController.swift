//
//  ViewController.swift
//  SageGlassApp
//
//  Created by Alex Baryschpolec on 1/20/17.
//  Copyright © 2017 DrexelSeniorDesign. All rights reserved.
//

import UIKit

var config: Config?
var survey: Survey = Survey.init()
var medals: Medals = Medals.init()

class HomeController: UIViewController {
    
    @IBOutlet weak var bronze: UILabel!
    @IBOutlet weak var silver: UILabel!
    @IBOutlet weak var gold: UILabel!
    @IBOutlet weak var beginSurvey: UIButton!
    @IBOutlet weak var sageControl: UIButton!
    @IBOutlet weak var podiumImg: UIImageView!
    @IBOutlet weak var sageLogo: UIImageView!
    @IBOutlet weak var chopLogo: UIImageView!
    @IBOutlet weak var gearIcon: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        config = Config.init()
        
        bronze.text = String(describing: medals.bronze)
        silver.text = String(describing: medals.silver)
        gold.text = String(describing: medals.gold)
        
        beginSurvey.layer.shadowOpacity = 0.7
        beginSurvey.layer.shadowOffset = CGSize(width: 3.0, height: 2.0)
        beginSurvey.layer.cornerRadius = 5
        
        sageControl.layer.shadowOpacity = 0.7
        sageControl.layer.shadowOffset = CGSize(width: 3.0, height: 2.0)
        sageControl.layer.cornerRadius = 5
        
        podiumImg.layer.shadowOpacity = 0.7
        podiumImg.layer.shadowOffset = CGSize(width: 3.0, height: 2.0)
        
        sageLogo.layer.shadowOpacity = 0.5
        sageLogo.layer.shadowOffset = CGSize(width: 3.0, height: 2.0)
        
        chopLogo.layer.shadowOpacity = 0.5
        chopLogo.layer.shadowOffset = CGSize(width: 3.0, height: 2.0)
        
        gearIcon.layer.shadowOpacity = 0.3
        gearIcon.layer.shadowOffset = CGSize(width: 3.0, height: 2.0)

        if !((config?.controlRoom)!){
            sageControl.isEnabled = false
            sageControl.isOpaque = false
            sageControl.isHidden = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func startSurvey(_ sender: Any) {
        
    }

    @IBAction func testMedal(_ sender: Any) {
        _ = medals.award();
        print(String(describing: medals.bronze))
        bronze.text = String(describing: medals.bronze)
        silver.text = String(describing: medals.silver)
        gold.text = String(describing: medals.gold)
    }
    @IBAction func testDH(_ sender: Any) {
        let dh:DataHandler = DataHandler.init()
        print(dh.getDataString(fileName: "settings.conf"))
    }
}

