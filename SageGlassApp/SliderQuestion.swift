//
//  SliderQuestion.swift
//  SageGlassApp
//
//  Created by Alex Baryschpolec on 3/12/17.
//  Copyright © 2017 DrexelSeniorDesign. All rights reserved.
//

import UIKit

class SliderQuestion: Question{
    
    override init(question: String){
        super.init(question: question);
    }
    
    override func saveMAnswer(mAnswer: Int){
        self.machineAnswer = mAnswer;
    }
}
