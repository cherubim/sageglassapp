//
//  ThankYouController.swift
//  SageGlassApp
//
//  Created by Alex Baryschpolec on 4/16/17.
//  Copyright © 2017 DrexelSeniorDesign. All rights reserved.
//

import UIKit

class ThankYouController: UIViewController {
    @IBOutlet weak var thankYouLabel: UILabel!
    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var medalImg: UIImageView!
    @IBOutlet weak var sageLogo: UIImageView!
    @IBOutlet weak var chopLogo: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let awarded = medals.award()
        
        if awarded == 0 {
            thankYouLabel.text = "Congratulations, and thank you for completing your survey! You have been awarded a bronze medal for completing this survey. Come back again today to receive a silver medal!"
            medalImg.image = UIImage(named: "bronze")!
        } else if awarded == 1 {
            thankYouLabel.text = "Congratulations, and thank you for completing your survey! You have been awarded a silver medal for completing this survey. Come back again today to receive a gold medal!"
            medalImg.image = UIImage(named: "silver")!
        } else {
            thankYouLabel.text = "Thank you for completing your survey! You have been awarded a gold medal for completing this survey!"
            medalImg.image = UIImage(named: "gold")!
        }
        
        sageLogo.layer.shadowOpacity = 0.5
        sageLogo.layer.shadowOffset = CGSize(width: 3.0, height: 2.0)
        
        chopLogo.layer.shadowOpacity = 0.5
        chopLogo.layer.shadowOffset = CGSize(width: 3.0, height: 2.0)
        
        homeButton.layer.shadowOpacity = 0.7
        homeButton.layer.shadowOffset = CGSize(width: 3.0, height: 2.0)
        homeButton.layer.cornerRadius = 5

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
