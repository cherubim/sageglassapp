//
//  Q4CollectionViewCell.swift
//  SageGlassApp
//
//  Created by Kaylee Stutts on 4/6/17.
//  Copyright © 2017 DrexelSeniorDesign. All rights reserved.
//

import UIKit

class Q4CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var mcIconView: UIImageView!
    
}
