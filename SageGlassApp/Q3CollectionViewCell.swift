//
//  Q3CollectionViewCell.swift
//  SageGlassApp
//
//  Created by Henry Yam on 4/9/17.
//  Copyright © 2017 DrexelSeniorDesign. All rights reserved.
//

import UIKit

class Q3CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var smileIconView: UIImageView!
    
    @IBOutlet weak var smileIconLabel: UILabel!
}
