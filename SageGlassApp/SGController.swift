//
//  SGController.swift
//  SageGlassApp
//
//  Created by Alex Baryschpolec on 4/2/17.
//  Copyright © 2017 DrexelSeniorDesign. All rights reserved.
//

import UIKit
import SwiftSocket

class SGController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    @IBOutlet weak var noTint: UIButton!
    @IBOutlet weak var fullTint: UIButton!
    @IBOutlet weak var mediumTint: UIButton!
    @IBOutlet weak var lightTint: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var tintLabel: UILabel!
    @IBOutlet var radioGroup: [UIButton]!
    @IBOutlet weak var cview: UICollectionView!
    
    @IBOutlet weak var progressLabel: UILabel!
    var minsLeft: Int = 16
    var timer: DispatchSourceTimer?
    
    let reuseIdentifier = "SGCell"
    var items_nodim = [UIImage(named: "SageDim1")!,
                 UIImage(named: "SageDim2")!,
                 UIImage(named: "SageDim3")!,
                 UIImage(named: "SageDim4")!,
                 UIImage(named: "SageDim5")!,
                 UIImage(named: "SageDim6")!,
                 UIImage(named: "SageDim7")!,
                 UIImage(named: "SageDim8")!,
                 UIImage(named: "SageDim9")!,
                 UIImage(named: "SageDim10")!,
                 UIImage(named: "SageDim11")!,
                 UIImage(named: "SageDim12")!]
    
    var items_lightdim = [UIImage(named: "lightdim1")!,
                  UIImage(named: "lightdim2")!,
                  UIImage(named: "lightdim3")!,
                  UIImage(named: "lightdim4")!,
                  UIImage(named: "lightdim5")!,
                  UIImage(named: "lightdim6")!,
                  UIImage(named: "lightdim7")!,
                  UIImage(named: "lightdim8")!,
                  UIImage(named: "lightdim9")!,
                  UIImage(named: "lightdim10")!,
                  UIImage(named: "lightdim11")!,
                  UIImage(named: "lightdim12")!]
    
    var items_meddim = [UIImage(named: "meddim1")!,
                  UIImage(named: "meddim2")!,
                  UIImage(named: "meddim3")!,
                  UIImage(named: "meddim4")!,
                  UIImage(named: "meddim5")!,
                  UIImage(named: "meddim6")!,
                  UIImage(named: "meddim7")!,
                  UIImage(named: "meddim8")!,
                  UIImage(named: "meddim9")!,
                  UIImage(named: "meddim10")!,
                  UIImage(named: "meddim11")!,
                  UIImage(named: "meddim12")!]
    
    var items_fulldim = [UIImage(named: "fulldim1")!,
                  UIImage(named: "fulldim2")!,
                  UIImage(named: "fulldim3")!,
                  UIImage(named: "fulldim4")!,
                  UIImage(named: "fulldim5")!,
                  UIImage(named: "fulldim6")!,
                  UIImage(named: "fulldim7")!,
                  UIImage(named: "fulldim8")!,
                  UIImage(named: "fulldim9")!,
                  UIImage(named: "fulldim10")!,
                  UIImage(named: "fulldim11")!,
                  UIImage(named: "fulldim12")!]
    
    var items_error = [UIImage(named: "SageDim1")!,
                         UIImage(named: "SageDim2")!,
                         UIImage(named: "SageDim3")!,
                         UIImage(named: "SageDim4")!,
                         UIImage(named: "SageDim5")!,
                         UIImage(named: "SageDim6")!,
                         UIImage(named: "SageDim7")!,
                         UIImage(named: "SageDim8")!,
                         UIImage(named: "SageDim9")!,
                         UIImage(named: "SageDim10")!,
                         UIImage(named: "SageDim11")!,
                         UIImage(named: "SageDim12")!]
    
    var states = repeatElement(-1, count: 12)
    var selectedValues = [Int]()
    var socket: TCPClient?

    override func viewDidLoad() {
        super.viewDidLoad()
    
        // Initialize Websocket
        socket = TCPClient(address: (config?.SIM_IP)!, port: (config?.SIM_PORT)!)
        
        // hide button labels from view
        noTint.titleLabel?.layer.opacity = 0.0;
        lightTint.titleLabel?.layer.opacity = 0.0;
        mediumTint.titleLabel?.layer.opacity = 0.0;
        fullTint.titleLabel?.layer.opacity = 0.0;
        
        backBtn.layer.shadowOpacity = 0.7
        backBtn.layer.shadowOffset = CGSize(width: 3.0, height: 2.0)
        backBtn.layer.cornerRadius = 5
        
        noTint.layer.shadowOpacity = 0.7
        noTint.layer.shadowOffset = CGSize(width: 3.0, height: 2.0)
        lightTint.layer.shadowOpacity = 0.7
        lightTint.layer.shadowOffset = CGSize(width: 3.0, height: 2.0)
        mediumTint.layer.shadowOpacity = 0.7
        mediumTint.layer.shadowOffset = CGSize(width: 3.0, height: 2.0)
        fullTint.layer.shadowOpacity = 0.7
        fullTint.layer.shadowOffset = CGSize(width: 3.0, height: 2.0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if UIDevice.current.orientation.isLandscape {
            AppUtility.lockOrientation(.landscape)
        } else {
            // Rotate to landscape and lock
            AppUtility.lockOrientation(.landscape, andRotateTo: .landscapeLeft)
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        AppUtility.lockOrientation(.all)

    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items_nodim.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // Load
        collectionView.delegate = self
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! ControlCellCollectionViewCell
        cell.img.image = self.items_nodim[indexPath.item]
        cell.layer.borderWidth = 3;
        cell.layer.borderColor = UIColor.black.cgColor
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.allowsMultipleSelection = true
        selectedValues.append(indexPath.item)
        // Add check mark to image
        let cell = collectionView.cellForItem(at: indexPath) as! ControlCellCollectionViewCell
        cell.checkedImg.image = UIImage(named: "check")
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath){
        let pos = selectedValues.index(of: indexPath.item)
        selectedValues.remove(at: pos!)
        //remove check mark from image
        let cell = collectionView.cellForItem(at: indexPath) as! ControlCellCollectionViewCell
        cell.checkedImg.image = nil
    }
    
    @IBAction func radioGroupClicked(_ sender: UIButton) {
        // Unhighlight all buttons
        unhighlightRadioGroup()
        
        // Highlight the one being clicked on
        highlightRadioGroup(button: sender)
        
        for elmnt in cview.indexPathsForSelectedItems!{
            let cell = cview.cellForItem(at: elmnt) as! ControlCellCollectionViewCell
            let index = elmnt.item
            switch(sender.title(for: .normal)!){
            case "No Tint":
                cell.img.image = items_nodim[index]
                cell.value = 0
            case "Light Tint":
                cell.img.image = items_lightdim[index]
                cell.value = 1
            case "Medium Tint":
                cell.img.image = items_meddim[index]
                cell.value = 2
            case "Full Tint":
                cell.img.image = items_fulldim[index]
                cell.value = 3
            default:
                cell.img.image = items_error[index]
                cell.value = -1
                // activate error state
            }
            
            cell.checkedImg.image = nil
            cview.deselectItem(at: elmnt, animated: false)
        }
    }
    
    // Set all 4 buttons in unselected state
    func unhighlightRadioGroup() {
        for button in radioGroup {
            button.isSelected = false
        }
    }
    
    // Set one button in the selected state
    func highlightRadioGroup(button : UIButton) {
        button.isSelected = true
    }

    @IBAction func sendMessage(_ sender: UIButton) {
        print("sendMessage called")
        // Check if we're connected
        guard let socket = socket else {
            // Stuff's gone wrong, enter error states
            print("Socket not connected")
            return
        }
        
        // Get the states of all selected items
        var tintLevel = 0x00
        switch(sender.title(for: .normal)!){
        case "No Tint":
            tintLevel = 0x00;
        case "Light Tint":
            tintLevel = 0x15;
        case "Medium Tint":
            tintLevel = 0x40
        case "Full Tint":
            tintLevel = 0x60
        default:
            tintLevel = 0x00
            // activate error state
        }
        
        for elmnt in selectedValues {
            print("Sending command")
            
            // Create websocket commands based on states of all items
            let payload = [UInt8(bitPattern: Int8(0x20)),
                           UInt8(bitPattern: Int8(0x00)),
                           UInt8(bitPattern: Int8(0x00)),
                           UInt8(bitPattern: Int8(0x00)),
                           0x0E,0x00,
                           0x02,0x82,
                           0xBE,0xEF,
                           0x08,
                           0x00,
                           0x00,
                           0x00,
                           UInt8(bitPattern: Int8(elmnt)),
                           0x01,
                           0x01,
                           0x00, UInt8(bitPattern: Int8(tintLevel))]
            let payloadAsData = Data(bytes: payload)
            
            // send each command in sequence
            switch socket.connect(timeout: 10){
            case .success:
                print("Websocket successfully connected")
                let res = socket.send(data: payloadAsData)
                switch res {
                case .success:
                    print("Message sent")
                    guard let resp = socket.read(1024*10) else {
                        //Stuff's gone wrong, but it matters that the command was sent.
                        print("Response fail")
                        return
                    }
                    for elmnt in resp{
                        print(elmnt)
                    }
                default:
                    // Stuffs gone wrong, enter error status.
                    print("Error occurred")
                }
            case .failure(let error):
                print("Error occurred: \(error)")
            }
        }
        
        // Set timer and notify user we're working
        minsLeft = 16
        timer = DispatchSource.makeTimerSource(queue: DispatchQueue(label: "DispatchQueue.main"))
        timer!.scheduleRepeating(deadline: .now(), interval: .seconds(60))
        for elmnt in cview.indexPathsForVisibleItems{
            if (selectedValues.contains(elmnt.item)) {
                print ("Starting animation")
                let cell = cview.cellForItem(at: elmnt) as! ControlCellCollectionViewCell
                cell.spinner.startAnimating()
            }
        }
        timer!.setEventHandler{
            self.minsLeft -= 1
            if self.minsLeft < 1{
                self.stopTimer()
            }
        }
        selectedValues = []
    }

    func stopTimer(){
        for elmnt in cview.indexPathsForVisibleItems{
            let cell = cview.cellForItem(at: elmnt) as! ControlCellCollectionViewCell
            cell.spinner.stopAnimating()
        }
        timer?.cancel()
        timer = nil
    }
    
    deinit {
        stopTimer()
    }

}
