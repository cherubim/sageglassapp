//
//  SurveyController.swift
//  SageGlassApp
//
//  Created by Alex Baryschpolec on 1/29/17.
//  Copyright © 2017 DrexelSeniorDesign. All rights reserved.
//

import UIKit

class Question2Controller: UIViewController {
    @IBOutlet weak var baseImgView: UIImageView!
    @IBOutlet weak var brightImgView: UIImageView!
    @IBOutlet weak var lightslider: UISlider!
    @IBOutlet weak var nextbtn: UIButton!
    @IBOutlet weak var sliderLabel: UILabel!
    @IBOutlet weak var q2Label: UILabel!
    
    var images: [UIImage]!
    
    var animatedImage: UIImage!
    
    var minimumValueImage: UIImage!
    
    var toPass:String! //Q1 data
    var answer = String()
    var failedAttempts = 0
    
    @IBAction func sliderValueChanged(_ sender: Any) {
        
        let currentValue = Int(lightslider.value) // Slider value is float by default, casting to Int to index array
        
        // Change slider label based on value on slider
        if currentValue >= 9 {
            sliderLabel.text = survey.questions[1].choices[(currentValue+1)/5]
        }
        else if currentValue < 9 && currentValue > 3 {
            sliderLabel.text = survey.questions[1].choices[1]
        }
        else {
            sliderLabel.text = survey.questions[1].choices[0]
        }
        
        // Adjust brightImgView.alpha based on distance from midpoint
        let midpoint = (lightslider.maximumValue + lightslider.minimumValue)/2
        let distPercent = abs(lightslider.value - midpoint) / (lightslider.maximumValue/2)
        brightImgView.alpha = CGFloat(distPercent)
        
        if (lightslider.value > midpoint){
            brightImgView.image = UIImage(named: "TooBright")!
            lightslider.setThumbImage(UIImage(named: "sun"), for: UIControlState.normal)
        } else if (lightslider.value < midpoint){
            brightImgView.image = UIImage(named: "TooDark")!
            lightslider.setThumbImage(UIImage(named: "moon"), for: UIControlState.normal)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        baseImgView.image = UIImage(named: "JustRight")!
        brightImgView.alpha = 0.5
        lightslider.minimumValueImage = UIImage(named: "moon_icon")
        lightslider.maximumValueImage = UIImage(named: "sun_icon")
        lightslider.setThumbImage(UIImage(named: "sun"), for: UIControlState.normal)
        
       let choices = ["Too Dark", "A Little Dark", "Just Right", "A Little Bright", "Too Bright"]
       survey.questions[1].choices = choices
        
        nextbtn.layer.shadowOpacity = 0.7
        nextbtn.layer.shadowOffset = CGSize(width: 3.0, height: 2.0)
        nextbtn.layer.cornerRadius = 5
    }
    
    func showAlert(title: String, message: String, actions: [String]){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        for i in 0..<actions.count{
            alert.addAction(UIAlertAction(title: actions[i], style: UIAlertActionStyle.default, handler: nil))
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func saveData(_ sender: Any) {
        let sliderText = sliderLabel.text!
        let q2 = survey.questions[1] as! SliderQuestion
        if(q2.choices.index(of: sliderText) == nil){
            if(failedAttempts == 0){
                showAlert(title: "Uh-oh!", message: "The answer that was received was not a part of the choices. Please enter in your answer again.", actions: ["OK"])
                failedAttempts += 1
            }
            else{
                showAlert(title: "Uh-oh!", message: "We're still getting a different answer than expected. Please let somebody know about this.", actions: ["OK"])
                q2.saveAnswers(answer: sliderText)
                self.performSegue(withIdentifier: "q2ToQ3", sender: self)
            }
        }
        else{
            q2.saveAnswers(answer: sliderText)
            self.performSegue(withIdentifier: "q2ToQ3", sender: self)
        }
    }
}
