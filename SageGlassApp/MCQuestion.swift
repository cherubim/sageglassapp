//
//  MCQuestion.swift
//  SageGlassApp
//
//  Created by Alex Baryschpolec on 3/12/17.
//  Copyright © 2017 DrexelSeniorDesign. All rights reserved.
//

import UIKit

class MCQuestion: Question{
    
    var machineAnswers = [Int()]
    var humanAnswers = [String()]
    
    override init(question: String){
        self.machineAnswers = [Int()];
        self.humanAnswers = [String()];
        super.init(question: question);
    }
    
    func saveMAnswers(hAnswers: [String]){
        for i in 0..<hAnswers.count{
            if((choices.index(of: hAnswers[i])) == nil){
                machineAnswers.append(-1)
            }
            else{
                if(i == 0){
                    machineAnswers[i] = choices.index(of: hAnswers[i])!
                }
                else{
                    machineAnswers.append(choices.index(of: hAnswers[i])!)
                }
            }
        }
        machineAnswerStr = formMachineAnswer()
    }
    
    func saveMAnswers(mAnswers: [Int]){
        machineAnswers = mAnswers
        machineAnswerStr = formMachineAnswer()
    }
    
    func saveHAnswers(hAnswers: [String]){
        self.humanAnswers = hAnswers;
        humanAnswerStr = formHumanAnswer()
    }
    
    func saveAnswers(answers: [String]){
        humanAnswers = answers
        saveMAnswers(hAnswers: answers)
        machineAnswerStr = formMachineAnswer()
        humanAnswerStr = formHumanAnswer()
    }
    
    override func formHumanAnswer() -> String {
        var humanAnswerString = String()
        for hAnswer in humanAnswers{
            humanAnswerString = humanAnswerString + hAnswer + ","
        }
        return humanAnswerString
    }
    
    override func formMachineAnswer() -> String {
        var machineAnswerString = String()
        for mAnswer in machineAnswers{
            machineAnswerString = machineAnswerString + String(mAnswer) + ","
        }
        return machineAnswerString
    }
    
    override func getAnswers() -> String {
        let answer = "\t" + machineAnswerStr + "\t" + humanAnswerStr
        return answer
    }
}
