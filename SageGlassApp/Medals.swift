//
//  Medals.swift
//  SageGlassApp
//
//  Created by Alex Baryschpolec on 4/12/17.
//  Copyright © 2017 DrexelSeniorDesign. All rights reserved.
//

import Foundation

// Stores variables related to medal collection.
class Medals {
    let medalFile = "medals.data"
    
    var bronze: Int //Number of days 1 survey has been completed
    var silver: Int //Number of days 2 surveys have been completed
    var gold: Int //Number of days 3 surveys have been completed
    var total: Int //bronze + silver + gold;
    var frequency: Int //How many times the survey has been taken.
    
    //Initialize all medals to be zero.
    init(){
        print("Medals.swift: Initializing")
        let dh:DataHandler = DataHandler.init()
        if dh.checkFileExists(fileName: medalFile){
            let inData = dh.getDataString(fileName: medalFile)
            let splitData: [String] = inData.components(separatedBy: "\n")
            var setDict: [String:String] = ["":""]
            for elmnt in splitData{
                let splitElmnt = elmnt.components(separatedBy: ":")
                setDict[splitElmnt[0]] = splitElmnt[1]
            }
            bronze = Int(setDict["bronze"]!)!;
            silver = Int(setDict["silver"]!)!;
            gold = Int(setDict["gold"]!)!;
            total = bronze + silver + gold
        } else {
            bronze = 0;
            silver = 0;
            gold = 0;
            total = 0;
        }
        frequency = 0;
        print("Medals.swift: Initialization complete")
    }
    
    // Awards a medal to the user.
    // Modifies bronze, silver, and gold depending on frequency.
    // Returns either 0, 1, or 2 to represent the medal rewarded if necessary.
    func award() -> Int{
        print("Medal awarded!")
        let awarded = frequency;
        switch frequency{
        case 0:
            bronze += 1;
            frequency += 1;
            total += 1
            break
        case 1:
            silver += 1;
            frequency += 1;
            break
        case 2:
            gold += 1;
            break
        default: break
        }
        total += 1;
        return awarded
    }
    
    func saveMedals(){
        var setString: String = "bronze:" + String(self.bronze)
        setString = setString + "\nsilver:" + String(self.silver)
        setString = setString + "\ngold:" + String(self.gold)
        //setString = setString + "\ncontrolRoom:" + (self.controlRoom ? "1" : "0")
        print(setString)
        let dh:DataHandler = DataHandler.init()
        dh.overwriteWith(subString: setString, fileName:medalFile);
    }
}
