//
//  Config.swift
//  SageGlassApp
//
//  Created by Alex Baryschpolec on 3/9/17.
//  Copyright © 2017 DrexelSeniorDesign. All rights reserved.
//

import UIKit

class Config: NSObject{
    var configFile: String = "settings.conf"
    
    var password: String
    var perDay: Int
    var startTime: Date //Use only time
    var endTime: Date
    var destEmail: String
    var outFile: String
    var SIM_IP: String
    var SIM_PORT: Int32
    var controlRoom: Bool
    
    override init(){
        print("Config.swift: Initializing")
        let dh:DataHandler = DataHandler.init()
        if dh.checkFileExists(fileName: configFile){
            print("Config.swift: settings.conf exists")
            let setString: String = dh.getDataString(fileName: configFile)
            let splitData: [String] = setString.components(separatedBy: "\n")
            var setDict: [String:String] = ["":""]
            for elmnt in splitData{
                let splitElmnt = elmnt.components(separatedBy: ":")
                setDict[splitElmnt[0]] = splitElmnt[1]
            }
            
            // Variables that can be set straight from the dictionary
            self.outFile = setDict["outFile"] ?? "data.tsv" // These are default values that can be changed
            self.password = setDict["password"] ?? "chopsuey"
            self.destEmail = setDict["destEmail"] ?? "henryyam25@gmail.com"
            self.perDay = 1
            
            // Setting start and end time
            self.startTime = Date()
            self.endTime = Date()
            
            // Are we in a room with SageGlass installed? If so, set all of these values.
            if setDict["controlRoom"] == "true" {
                self.controlRoom = true;
                self.SIM_IP = setDict["SIM_IP"] ?? "192.168.0.50"
                if setDict["SIM_PORT"] != nil {
                    self.SIM_PORT = Int32(setDict["SIM_PORT"]!)!
                } else {
                    self.SIM_PORT = 8080
                }
            } else {
                self.controlRoom = false
                self.SIM_IP = "192.168.0.50"
                self.SIM_PORT = 8080
            }
            super.init()
        } else {
            print("Config.swift: settings.conf does not exist")
            self.password = "chopsuey"
            self.perDay = 1
            self.startTime = Date()
            self.endTime = Date()
            self.destEmail = "henryyam25@gmail.com"
            self.outFile = "data.tsv"
            self.SIM_IP = "192.168.0.50"
            self.SIM_PORT = 8080
            self.controlRoom = true
            super.init()
            saveSettings()
        }
    }
    
    func saveSettings(){
        var setString: String = "password:" + self.password
        setString = setString + "\nperDay:" + String(self.perDay)
        setString = setString + "\ndestEmail:" + self.destEmail
        setString = setString + "\noutFile:" + self.outFile
        setString = setString + "\nSIM_IP:" + self.SIM_IP
        setString = setString + "\nSIM_PORT:" + String(describing:self.SIM_PORT)
        setString = setString + "\ncontrolRoom:" + (self.controlRoom ? "true" : "false")
        print(setString)
        let dh:DataHandler = DataHandler.init()
        dh.overwriteWith(subString: setString, fileName:"settings.conf");
    }
    
    func setDefault(){
        // Does not reset email, on purpose.
        
        self.password = "chopsuey"
        self.perDay = 1
        self.startTime = Date()
        self.endTime = Date()
        self.outFile = "data.tsv"
        self.SIM_IP = "192.168.0.50"
        self.SIM_PORT = 8080
        self.controlRoom = false
    }
}
