//
//  SettingsTableViewCell.swift
//  SageGlassApp
//
//  Created by Henry Yam on 3/12/17.
//  Copyright © 2017 DrexelSeniorDesign. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBOutlet weak var spdLabel: UILabel!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
