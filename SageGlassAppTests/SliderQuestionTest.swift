//
//  Question1ControllerTest.swift
//  SageGlassApp
//
//  Created by Henry Yam on 5/7/17.
//  Copyright © 2017 DrexelSeniorDesign. All rights reserved.
//

import XCTest
@testable import SageGlassApp

class SliderQuestionTest: Test {
    
    var survey = Survey()
    
    func setup() -> SliderQuestion{
        let q1 = survey.questions[0] as! SliderQuestion
        q1.choices = ["Too Cold", "A Little Cold", "Just Right", "A Little Hot", "Too Hot"]
        return q1
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    override func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    override func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testq1DataSavingCorrectInput(){
        let q1 = setup()
        q1.saveAnswers(answer: "A Little Hot")
        XCTAssertEqual(q1.machineAnswer, 3)
    }
    
    func testq1DataSavingIncorrectInput(){
        let q1 = setup()
        q1.saveAnswers(answer: "aosidhfosiadf")
        XCTAssertEqual(q1.machineAnswer, -1)
    }
    
    override func testMain(){
        testq1DataSavingCorrectInput()
        testq1DataSavingIncorrectInput()
    }
}
