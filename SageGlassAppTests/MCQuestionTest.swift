//
//  MCQuestionTest.swift
//  SageGlassApp
//
//  Created by Henry Yam on 5/14/17.
//  Copyright © 2017 DrexelSeniorDesign. All rights reserved.
//

import XCTest
@testable import SageGlassApp

class MCQuestionTest: Test {
    
    let survey = Survey()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    func setup()->MCQuestion{
        let q4 = survey.questions[3] as! MCQuestion
        q4.choices = [
            "I closed my eyes",
            "I or a family member turned the lights on",
            "I or a family member turned the lights off",
            "I or a family member adjusted the blinds/adjusted the tint of glass",
            "I moved my position in the room",
            "I and my family members did nothing"]
        return q4
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    override func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    override func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testq1DataSavingCorrectInput(){
        let q1 = setup()
        q1.saveAnswers(answer: "A Little Hot")
        XCTAssertEqual(q1.machineAnswer, 3)
    }
    
    func testq1DataSavingIncorrectInput(){
        let q1 = setup()
        q1.saveAnswers(answer: ";oihefqihureglhiuads")
        XCTAssertEqual(q1.machineAnswer, -1)
    }
    
    override func testMain(){
        testq1DataSavingIncorrectInput()
        testq1DataSavingCorrectInput()
    }
}
