//
//  AllTests.swift
//  SageGlassApp
//
//  Created by Henry Yam on 5/15/17.
//  Copyright © 2017 DrexelSeniorDesign. All rights reserved.
//

import XCTest

class AllTests: XCTestSuite {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testAll(){
        var allTests = [Test]()
        let sqt = SliderQuestionTest()
        let mcqt = MCQuestionTest()
        let cfgt = ConfigTest()
        let mt = MedalsTest()
        allTests.append(sqt)
        allTests.append(mcqt)
        allTests.append(cfgt)
        allTests.append(mt)
        for test in allTests{
            test.testMain()
        }
    }
    
}
