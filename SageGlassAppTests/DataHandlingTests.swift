//
//  DataHandlingTests.swift
//  SageGlassApp
//
//  Created by Henry Yam on 4/13/17.
//  Copyright © 2017 DrexelSeniorDesign. All rights reserved.
//

import XCTest
@testable import SageGlassApp

class DataHandlingTests: Test {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    override func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    override func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testSaveAndRead(){
        var dh = DataHandler()
        dh.file = "test.csv"
        let string = "1\t2\t3"
        dh.save(subString: string, fileName: "test.csv")
        let actual = dh.getDataString(fileName: "test.csv")
        let expected = string
        print(actual)
        XCTAssertEqual(actual, expected)
    }
}
